// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

contract StudentRecord{
    uint public studentsCount = 0;

    constructor() {
        addStudent( 12220096, "Ugyen Tshering");
        }

    // Model a Student
    struct Student {
        uint _id;
        uint sid;
        string name;
        bool graduated;
    }
    mapping(uint => Student) public students;

    // Events
   event addStudentEvent (
        uint _id,
        uint indexed sid,
        string name,
        bool graduated,
        bool isDuplicate
    );


    event markGraduatedEvent (
    int indexed sid
    );
    
    //Change graduation status of student
    function markGraduated(uint256 _id) public returns (Student memory) {
        students[_id].graduated = true;
        // trigger create event
        emit markGraduatedEvent(int256(_id));
        return students[_id];
    }
    
    //Fetch student info from storage
    function findStudent(uint256 _id) public view returns (Student memory) {
        return students[_id];
    }
    function isStudentDuplicate(uint sid) public view returns (bool) {
        for (uint i = 1; i <= studentsCount; i++) {
            if (students[i].sid == sid) {
                return true;
            }
        }
        return false;
        }

    //Create and add student to storage
   function addStudent(uint _studentNumber, string memory _name) public returns (Student memory) {
    if (isStudentDuplicate(_studentNumber)) {
        // Student with the same ID already exists
        emit addStudentEvent(studentsCount, _studentNumber, _name, false, true);
        revert("Student with the same ID already exists");
    }

    studentsCount++;
    students[studentsCount] = Student(studentsCount, _studentNumber, _name, false);

    // trigger create event
    emit addStudentEvent(studentsCount, _studentNumber, _name, false, false);
    return students[studentsCount];
    }
}

