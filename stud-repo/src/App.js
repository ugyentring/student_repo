import React, { Component } from 'react';
import Web3 from 'web3';
import './App.css';
import StudentRecord from './components/StudentRecord';

import { STUDENTRECORD_ABI, STUDENTRECORD_ADDRESS } from './abi/config_studentrecord';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      account: '',
      studentCount: 0,
      students: [],
      loading: true,
    };
    this.markGraduated = this.markGraduated.bind(this)
  }
  addStudent = async (sid, name) => {
    this.setState({ loading: true });

    try {
      // Check for duplicate student on the smart contract
      const isDuplicate = await this.state.studentRecord.methods.isStudentDuplicate(sid).call();

      if (isDuplicate) {
        alert("Student with the same ID already exists.");
        this.setState({ loading: false });
        return;
      }

      const gasEstimate = await this.state.studentRecord.methods.addStudent(sid, name)
        .estimateGas({ from: this.state.account });

      alert(`Estimated gas usage: ${gasEstimate}`);

      await this.state.studentRecord.methods.addStudent(sid, name)
        .send({
          from: this.state.account,
          gas: gasEstimate,
        });

      this.setState({ loading: false });
      this.loadBlockchainData();
    } catch (error) {
      console.error("Error estimating gas or sending transaction:", error);
      this.setState({ loading: false });
    }
  }

  markGraduated = async (sid) => {
    this.setState({ loading: true });

    try {
      const gasEstimate = await this.state.studentRecord.methods.markGraduated(sid)
        .estimateGas({ from: this.state.account });

      console.log(`Estimated gas usage: ${gasEstimate}`);

      await this.state.studentRecord.methods.markGraduated(sid)
        .send({
          from: this.state.account,
          gas: gasEstimate,
        });

      this.setState({ loading: false });
      this.loadBlockchainData();
    } catch (error) {
      console.error("Error estimating gas or sending transaction:", error);
      this.setState({ loading: false });
    }
  }

  async componentDidMount() {
    try {
      if (window.ethereum) {
        const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });

        if (accounts && accounts.length > 0) {
          // Account access granted, proceed to load blockchain data
          this.loadBlockchainData();
        } else {
          console.error('Account access not granted.');
        }
      } else {
        console.error('MetaMask not found. Please install and configure MetaMask.');
      }
    } catch (error) {
      console.error(error.message);
    }
  }


  async loadBlockchainData() {
    try {
      const web3 = new Web3(Web3.givenProvider || "http://localhost:7545");
      const accounts = await web3.eth.getAccounts();
      this.setState({ account: accounts[1] });

      const studentRecord = new web3.eth.Contract(STUDENTRECORD_ABI, STUDENTRECORD_ADDRESS);
      this.setState({ studentRecord });

      const studentCount = await studentRecord.methods.studentsCount().call();
      this.setState({ studentCount });

      const students = [];
      for (var i = 1; i <= studentCount; i++) {
        const student = await studentRecord.methods.students(i).call();
        students.push(student);
      }
      this.setState({ students, loading: false });
    } catch (error) {
      console.error(error.message);
    }
  }

  render() {
    return (
      <div className="container">
        <h1>Student Record System</h1>
        <p>Your account: {this.state.account}</p>
        <StudentRecord addStudent={this.addStudent} />
        <ul id="studentList" className="list-unstyled">
          {this.state.students.map((student, key) => (
            <li className="list-group-item checkbox" key={key}>
              <span className="name alert">
                {student._id}. {student.sid} {student.name}
              </span>
              <input
                className="form-check-input"
                type="checkbox"
                name={student._id}
                defaultChecked={student.graduated}
                disabled={student.graduated}
                ref={(input) => {
                  this.checkbox = input;
                }} onClick={(event) => {
                  this.markGraduated(event.currentTarget.name)
                }
                } />
              <label className="form-check-label">Graduated</label>
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

export default App;


